package developer.ink1804.intechglobaltest.network.api

import developer.ink1804.intechglobaltest.network.models.TrackListResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ITunesApiInterface {

    @GET("search")
    fun search(@Query("term") query: String): Observable<TrackListResponse>
}