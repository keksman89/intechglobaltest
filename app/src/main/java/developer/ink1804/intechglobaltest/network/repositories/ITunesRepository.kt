package developer.ink1804.intechglobaltest.network.repositories

import developer.ink1804.intechglobaltest.network.api.ITunesApiInterface
import developer.ink1804.intechglobaltest.network.models.TrackListResponse
import developer.ink1804.intechglobaltest.utils.rx.applySchedulers
import io.reactivex.Observable
import javax.inject.Inject

class ITunesRepository @Inject constructor() {

    @Inject
    lateinit var apiManager: ITunesApiInterface

    fun searchTracks(query: String): Observable<TrackListResponse> {
        return apiManager.search(query).applySchedulers()
    }
}