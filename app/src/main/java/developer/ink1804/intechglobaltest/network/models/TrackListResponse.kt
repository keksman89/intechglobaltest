package developer.ink1804.intechglobaltest.network.models

import com.google.gson.annotations.SerializedName


data class TrackListResponse(
        @SerializedName("resultCount")
        var resultCount: Int,

        @SerializedName("results")
        var results: List<Track>
)