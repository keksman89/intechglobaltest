package developer.ink1804.intechglobaltest.network.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Track(
        @SerializedName("artistName")
        var artistName: String,

        @SerializedName("trackName")
        var trackName: String,

        @SerializedName("artworkUrl100")
        var thumbnailUrl: String,

        @SerializedName("previewUrl")
        var previewUrl: String,

        @SerializedName("trackTimeMillis")
        var trackLength: Int

) : Serializable