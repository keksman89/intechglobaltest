package developer.ink1804.intechglobaltest.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import developer.ink1804.intechglobaltest.R
import developer.ink1804.intechglobaltest.ui.track_list.TrackListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initUI()
    }

    private fun initUI() {
        supportFragmentManager.beginTransaction()
                .add(R.id.fragmentContainer, TrackListFragment())
                .commit()
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .addToBackStack(fragment::class.java.simpleName)
                .commit()
    }

    fun popFragment() {
        supportFragmentManager.popBackStack()
    }
}
