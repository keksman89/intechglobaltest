package developer.ink1804.intechglobaltest.ui.player

import android.content.Context.AUDIO_SERVICE
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.presenter.InjectPresenter
import com.bumptech.glide.Glide
import developer.ink1804.intechglobaltest.AppApplication
import developer.ink1804.intechglobaltest.R
import developer.ink1804.intechglobaltest.network.models.Track
import developer.ink1804.intechglobaltest.presentation.presenter.PlayerPresenter
import developer.ink1804.intechglobaltest.presentation.view.PlayerView
import developer.ink1804.intechglobaltest.ui.common.moxy.MvpKtxFragment
import developer.ink1804.intechglobaltest.utils.rx.RxMediaPlayer
import developer.ink1804.intechglobaltest.utils.millisecondsToTime
import kotlinx.android.synthetic.main.frg_player.*
import kotlinx.android.synthetic.main.frg_track_list.toolbar
import timber.log.Timber

class PlayerFragment : MvpKtxFragment(), PlayerView, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener {

    companion object {
        private const val TRACK_EXTRA = "trackExtra"

        fun newInstance(track: Track): PlayerFragment {
            val fragment = PlayerFragment()
            val args = Bundle()

            args.putSerializable(TRACK_EXTRA, track)
            fragment.arguments = args

            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: PlayerPresenter

    private var mediaPlayer: RxMediaPlayer? = null
    private var am: AudioManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.inject(this)
        super.onCreate(savedInstanceState)

        arguments?.let {
            if (it.containsKey(TRACK_EXTRA)) {
                presenter.putTrack(it.getSerializable(TRACK_EXTRA) as Track)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frg_player, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initUI()
        am = activity?.getSystemService(AUDIO_SERVICE) as AudioManager
    }

    private fun initUI() {
        ivPlay.setOnClickListener {
            presenter.onPlayClick()
        }
    }

    override fun showTrackInfo(track: Track) {
        toolbar.title = track.trackName
        toolbar.subtitle = track.artistName
        progressBar.max = track.trackLength
        tvDuration.text = millisecondsToTime(track.trackLength.toLong())
        tvCurrentPosition.text = getString(R.string.zero_current_position)

        Glide.with(this).load(track.thumbnailUrl).into(ivThumbnail)
    }

    override fun startPlayingTrack(url: String) {
        releaseMP()
        mediaPlayer = RxMediaPlayer()
        mediaPlayer?.run {
            this.setDataSource(url)
            this.setAudioAttributes(AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build())

            this.setOnPreparedListener(this@PlayerFragment)
            this.prepareAsync()
            this.setOnCompletionListener(this@PlayerFragment)
            presenter.addPositionListener(this.startListenProgressChanges())
        }
    }

    override fun resumePlayingTrack() {
        mediaPlayer?.let {
            if (!it.isPlaying) {
                it.start()
                presenter.addPositionListener(it.startListenProgressChanges())
            }
        }
    }

    override fun pausePlayingTrack() {
        mediaPlayer?.let {
            if (it.isPlaying) {
                it.pause()
            }
        }
    }

    override fun setPlayButtonSrc(iconRes: Int) {
        ivPlay.setImageResource(iconRes)
    }

    private fun releaseMP() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer!!.release()
                mediaPlayer = null
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    override fun onPrepared(mp: MediaPlayer?) {
        mp?.start()
    }

    override fun onCompletion(mp: MediaPlayer?) {
        presenter.stopPlaying()
    }

    override fun stopPlayingTrack() {
        progressBar.progress = 0
        tvCurrentPosition.text = getString(R.string.zero_current_position)
    }

    override fun setProgress(progress: Int) {
        progressBar.progress = progress
        tvCurrentPosition.text = millisecondsToTime(progress.toLong())
        Timber.wtf(progress.toString())
    }

    override fun onPause() {
        super.onPause()
        presenter.pausePlaying()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.stopPlaying()
        releaseMP()
    }
}