package developer.ink1804.intechglobaltest.ui.track_list

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import developer.ink1804.intechglobaltest.presentation.view.VhTrackView
import kotlinx.android.synthetic.main.vh_track.view.*

class VhTrack(itemView: View) : RecyclerView.ViewHolder(itemView), VhTrackView {

    override fun setTrackName(name: String) {
        itemView.tvName.text = name
    }

    override fun setArtist(artist: String) {
        itemView.tvArtist.text = artist
    }

    override fun loadThumbnail(thumbnailUrl: String) {
        Glide.with(itemView).load(thumbnailUrl).into(itemView.ivThumbnail)
    }

    override fun setOnClickListener(listener: View.OnClickListener) {
        itemView.setOnClickListener(listener)
    }
}