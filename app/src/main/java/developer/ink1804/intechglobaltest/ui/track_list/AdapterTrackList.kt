package developer.ink1804.intechglobaltest.ui.track_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import developer.ink1804.intechglobaltest.R
import developer.ink1804.intechglobaltest.presentation.presenter.TrackListPresenter

class AdapterTrackList(var presenter: TrackListPresenter) : RecyclerView.Adapter<VhTrack>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VhTrack {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.vh_track, parent, false)
        return VhTrack(view)
    }

    override fun getItemCount(): Int = presenter.getTrackListSize()

    override fun onBindViewHolder(holder: VhTrack, position: Int) {
        presenter.bindTrack(holder, position)
    }
}