package developer.ink1804.intechglobaltest.ui.track_list

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import developer.ink1804.intechglobaltest.AppApplication
import developer.ink1804.intechglobaltest.R
import developer.ink1804.intechglobaltest.network.models.Track
import developer.ink1804.intechglobaltest.presentation.presenter.TrackListPresenter
import developer.ink1804.intechglobaltest.presentation.view.TrackListView
import developer.ink1804.intechglobaltest.ui.MainActivity
import developer.ink1804.intechglobaltest.ui.player.PlayerFragment
import developer.ink1804.intechglobaltest.ui.common.moxy.MvpKtxFragment
import kotlinx.android.synthetic.main.frg_track_list.*
import kotlinx.android.synthetic.main.frg_track_list.toolbar
import javax.inject.Inject

class TrackListFragment : MvpKtxFragment(), TrackListView {

    @Inject
    @InjectPresenter
    lateinit var presenter: TrackListPresenter

    @ProvidePresenter
    fun providePresenter(): TrackListPresenter {
        return presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AppApplication.instance.applicationComponent.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.frg_track_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        toolbar.title = getString(R.string.title_track_list)
        recyclerView.layoutManager = LinearLayoutManager(activity)
        presenter.addSearchSubscription(etSearch.getTextWatcherObservable())
    }

    override fun showTrackList(tracks: List<Track>) {
        recyclerView.adapter = AdapterTrackList(presenter)
    }

    override fun onTrackClick(track: Track) {
        val fragment = PlayerFragment.newInstance(track)
        (activity as MainActivity).replaceFragment(fragment)
    }

    override fun updateTrackList() {
        recyclerView.adapter?.notifyDataSetChanged()
    }

    override fun showProgress() {
        progressBar.visibility = View.VISIBLE
        recyclerView.visibility = View.GONE
    }

    override fun hideProgress() {
        progressBar.visibility = View.GONE
        recyclerView.visibility = View.VISIBLE
    }
}