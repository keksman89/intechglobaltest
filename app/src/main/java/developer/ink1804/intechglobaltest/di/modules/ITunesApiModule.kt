package developer.ink1804.intechglobaltest.di.modules

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import developer.ink1804.intechglobaltest.network.api.ITunesApiInterface
import developer.ink1804.intechglobaltest.network.api.iTunesUrl
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [OkHttpClientModule::class])
class ITunesApiModule {

    @Singleton
    @Provides
    fun provideITunesApiInterface(retrofit: Retrofit): ITunesApiInterface {
        return retrofit.create(ITunesApiInterface::class.java)
    }

    @Singleton
    @Provides
    fun retrofit(okHttpClient: OkHttpClient, gsonConverterFactory: GsonConverterFactory): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(iTunesUrl)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
    }

    @Provides
    fun gson(): Gson {
        val gsonBuilder = GsonBuilder().setDateFormat("")
        return gsonBuilder.create()
    }


    @Provides
    fun gsonConvertFactory(gson: Gson): GsonConverterFactory {
        return GsonConverterFactory.create(gson)
    }

}
