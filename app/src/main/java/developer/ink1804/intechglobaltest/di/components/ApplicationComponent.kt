package developer.ink1804.intechglobaltest.di.components

import dagger.Component
import developer.ink1804.intechglobaltest.di.modules.ITunesApiModule
import developer.ink1804.intechglobaltest.ui.MainActivity
import developer.ink1804.intechglobaltest.ui.player.PlayerFragment
import developer.ink1804.intechglobaltest.ui.track_list.TrackListFragment
import javax.inject.Singleton

@Singleton
@Component(modules = [ITunesApiModule::class])
interface ApplicationComponent {

    fun inject(activity: MainActivity)

    fun inject(fragment: TrackListFragment)
    fun inject(fragment: PlayerFragment)

}