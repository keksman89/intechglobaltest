package developer.ink1804.intechglobaltest.di.annotations

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext
