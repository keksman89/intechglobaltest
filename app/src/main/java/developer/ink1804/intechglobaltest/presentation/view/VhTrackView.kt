package developer.ink1804.intechglobaltest.presentation.view

import android.view.View

interface VhTrackView {

    fun setTrackName(name: String)
    fun setArtist(artist: String)
    fun loadThumbnail(thumbnailUrl: String)

    fun setOnClickListener(listener: View.OnClickListener)
}