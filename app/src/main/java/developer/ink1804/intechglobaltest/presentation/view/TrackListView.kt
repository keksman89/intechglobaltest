package developer.ink1804.intechglobaltest.presentation.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import developer.ink1804.intechglobaltest.network.models.Track

@StateStrategyType(SingleStateStrategy::class)
interface TrackListView : MvpView {

    @StateStrategyType(SkipStrategy::class)
    fun showTrackList(tracks: List<Track>)

    @StateStrategyType(SkipStrategy::class)
    fun onTrackClick(track: Track)

    fun updateTrackList()

    fun showProgress()
    fun hideProgress()
}