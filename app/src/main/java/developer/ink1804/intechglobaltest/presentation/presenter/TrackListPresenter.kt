package developer.ink1804.intechglobaltest.presentation.presenter

import android.view.View
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.intechglobaltest.network.models.Track
import developer.ink1804.intechglobaltest.network.repositories.ITunesRepository
import developer.ink1804.intechglobaltest.presentation.view.TrackListView
import developer.ink1804.intechglobaltest.presentation.view.VhTrackView
import developer.ink1804.intechglobaltest.utils.rx.disposedBy
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class TrackListPresenter @Inject constructor() : MvpPresenter<TrackListView>() {

    @Inject
    lateinit var repository: ITunesRepository

    private var disposeBag: CompositeDisposable = CompositeDisposable()

    private var tracks: MutableList<Track> = ArrayList()

    fun getTrackListSize(): Int {
        return tracks.size
    }

    fun bindTrack(holder: VhTrackView, position: Int) {
        val track = tracks[position]
        holder.setTrackName(track.trackName)
        holder.setArtist(track.artistName)
        holder.loadThumbnail(track.thumbnailUrl)

        holder.setOnClickListener(View.OnClickListener {
            this.viewState.onTrackClick(track)
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeBag.dispose()
    }

    fun addSearchSubscription(textWatcherObservable: Observable<String>) {
        textWatcherObservable
                .distinctUntilChanged()
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .filter {
                    val isInputCorrect = it.length >= 5

                    if (!isInputCorrect) {
                        tracks.clear()
                        viewState.updateTrackList()
                        viewState.hideProgress()
                    }

                    return@filter isInputCorrect
                }
                .flatMap {
                    viewState.showProgress()
                    return@flatMap repository.searchTracks(it)
                }.subscribe({
                    this.tracks = it.results.toMutableList()
                    viewState.showTrackList(it.results)
                    viewState.hideProgress()
                }, {
                    Timber.wtf(it)
                }).disposedBy(disposeBag)
    }
}