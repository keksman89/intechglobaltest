package developer.ink1804.intechglobaltest.presentation.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import developer.ink1804.intechglobaltest.R
import developer.ink1804.intechglobaltest.network.models.Track
import developer.ink1804.intechglobaltest.presentation.view.PlayerView
import developer.ink1804.intechglobaltest.utils.PlayerState
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

@InjectViewState
class PlayerPresenter : MvpPresenter<PlayerView>() {

    lateinit var track: Track
    var playerState: PlayerState = PlayerState.Default
    var currentPositionDisposable: Disposable? = null

    fun putTrack(track: Track) {
        this.track = track

        viewState.showTrackInfo(track)
    }

    fun onPlayClick() {
        when (playerState) {
            PlayerState.Default -> {
                startPlaying()
            }
            PlayerState.Paused -> {
                resumePlaying()
            }
            PlayerState.Playing -> {
                pausePlaying()
            }
        }
    }

    fun addPositionListener(startListenProgressChanges: Observable<Int>) {
        currentPositionDisposable = startListenProgressChanges
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    viewState.setProgress(it)
                }
    }

    private fun startPlaying() {
        viewState.startPlayingTrack(track.previewUrl)
        viewState.setPlayButtonSrc(R.drawable.ic_pause)
        playerState = PlayerState.Playing
    }

    fun pausePlaying() {
        viewState.pausePlayingTrack()
        viewState.setPlayButtonSrc(R.drawable.ic_play)
        currentPositionDisposable?.dispose()
        playerState = PlayerState.Paused
    }

    private fun resumePlaying() {
        viewState.resumePlayingTrack()
        viewState.setPlayButtonSrc(R.drawable.ic_pause)
        playerState = PlayerState.Playing
    }

    fun stopPlaying() {
        viewState.stopPlayingTrack()
        viewState.setPlayButtonSrc(R.drawable.ic_play)
        currentPositionDisposable?.dispose()
        playerState = PlayerState.Default
    }
}

