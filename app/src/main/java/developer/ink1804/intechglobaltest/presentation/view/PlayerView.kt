package developer.ink1804.intechglobaltest.presentation.view

import androidx.annotation.DrawableRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import developer.ink1804.intechglobaltest.network.models.Track

@StateStrategyType(SingleStateStrategy::class)
interface PlayerView : MvpView {

    fun showTrackInfo(track: Track)

    fun startPlayingTrack(url: String)
    fun resumePlayingTrack()
    fun pausePlayingTrack()
    fun stopPlayingTrack()

    fun setPlayButtonSrc(@DrawableRes iconRes: Int)

    fun setProgress(progress: Int)
}