package developer.ink1804.intechglobaltest.utils.rx

import android.content.Context
import androidx.appcompat.widget.AppCompatEditText
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class RxEditText @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = android.R.attr.editTextStyle
) : AppCompatEditText(context, attrs, defStyleAttr) {


    fun getTextWatcherObservable(): Observable<String> {
        val subject = PublishSubject.create<String>()

        val textWatcher = object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}

            override fun afterTextChanged(editable: Editable) {
                subject.onNext(editable.toString())
            }
        }

        this.addTextChangedListener(textWatcher)

        return subject
    }

}