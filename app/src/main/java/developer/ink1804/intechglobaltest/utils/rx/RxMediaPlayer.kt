package developer.ink1804.intechglobaltest.utils.rx

import android.media.MediaPlayer
import io.reactivex.Observable
import java.util.concurrent.TimeUnit


class RxMediaPlayer : MediaPlayer() {

    fun startListenProgressChanges(): Observable<Int> {
        return Observable.interval(500, TimeUnit.MILLISECONDS)
                .map {
                    return@map this@RxMediaPlayer.currentPosition
                }
    }
}

