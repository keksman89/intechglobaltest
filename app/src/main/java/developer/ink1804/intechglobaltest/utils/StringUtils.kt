package developer.ink1804.intechglobaltest.utils

import java.util.concurrent.TimeUnit

fun millisecondsToTime(millis: Long): String {
    return String.format("%d:%d",
            TimeUnit.MILLISECONDS.toMinutes(millis),
            TimeUnit.MILLISECONDS.toSeconds(millis) -
                    TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
    )
}