package developer.ink1804.intechglobaltest.utils

enum class PlayerState {
    Default, Paused, Playing
}