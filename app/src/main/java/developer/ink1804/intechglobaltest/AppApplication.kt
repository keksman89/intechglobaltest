package developer.ink1804.intechglobaltest

import android.app.Application
import developer.ink1804.intechglobaltest.di.components.ApplicationComponent
import developer.ink1804.intechglobaltest.di.components.DaggerApplicationComponent
import timber.log.Timber

class AppApplication : Application() {

    companion object {
        lateinit var instance: AppApplication
    }

    lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        applicationComponent = DaggerApplicationComponent.builder()
                .build()

        super.onCreate()

        instance = this

        Timber.plant(Timber.DebugTree())
    }
}